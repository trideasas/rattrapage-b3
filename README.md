# Examen
## Première étape
* Vous devez corriger tous les TODO dans le fichier main.py

## Deuxième étape
* Ajoutez une route dynamique /dynamic/ permettant d'afficher l'élèment dynamique.

## Questions
* À quoi servent les cookies dans les divers exemples que nous avons vus en TP ?
* Expliquez, avec vos mots, quel est l'intérêt d'un architecture micro-services.
* Quels sont les différences entre les méthodes POST, GET et DELETE

## À la fin de votre examen
* Retournez, zippé au format RATTRAPAGE-B3-VOTRENOM.zip tout votre dossier avec vos modifications, n'oubliez rien !